module.exports = {
    extends: ['airbnb-typescript'],
    parserOptions: {
        project: './tsconfig.json',
    },
    rules: {
        'max-len': ['error', 140],
        '@typescript-eslint/camelcase': 0,
        '@typescript-eslint/indent': ['error', 4],
        'react/jsx-indent': ['error', 4],
        'react/jsx-indent-props': ['error', 4], 
        'react/prop-types': 0, // No need for prop type validation, TypeScript to the rescue!
    },
};