declare module '*.scss' {
    const content: {[className: string]: string};
    export default content;
}

declare module '*.png' {
    const value: any;
    export = value;
}

declare module '*.gif' {
    const value: any;
    export = value;
}

declare module 'worker-loader!*' {
    class WebpackWorker extends Worker {
        constructor();
    }

    export default WebpackWorker;
}
