interface Manager {
    readonly name: string;
    readonly description: string;
    readonly price: number;
}
