interface Config {
    readonly currency: string;
    readonly startMoney: number;
    readonly maxBusinesses: number;
    readonly revenueIncrease: number;
    readonly priceIncrease: number;
    readonly businesses: ReadonlyArray<Business>;
}
