import bank from './bank.png';
import casino from './casino.png';
import coffee from './coffee.png';
import movie from './movie.png';
import nuclear from './nuclear.png';
import pizzeria from './pizzeria.png';
import store from './store.png';
import whiskey from './whiskey.png';

export default {
    bank,
    casino,
    coffee,
    movie,
    nuclear,
    pizzeria,
    store,
    whiskey,
};
