import getConfig from './config';

const { revenueIncrease } = getConfig();

const calculateEarnings = (
    revenue: number,
    count: number,
): number => revenue + (revenue * (revenueIncrease * (count - 1)));

const abvNumber = (num: number): string => {
    if (num < 1e3) return num.toString();
    if (num >= 1e3 && num < 1e6) return `${(num / 1e3).toFixed(0)}K`;
    if (num >= 1e6 && num < 1e9) return `${(num / 1e6).toFixed(2)}M`;
    if (num >= 1e9 && num < 1e12) return `${(num / 1e9).toFixed(4)}B`;

    return `${(num / 1e12).toFixed(6)}T`;
};

export {
    calculateEarnings,
    abvNumber,
};
