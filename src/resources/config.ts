import config from '../../config.json';

const getConfig = (): Config => config;

export default getConfig;
