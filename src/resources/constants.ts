export enum Views {
    BUSINESSES,
    MANAGERS,
    SETTINGS,
}

export enum MessageTypes {
    INIT,
    LOADING,
    SET_MONEY,
    SET_BUSINESSES,
    ADD_BUSINESS,
    RUN_BUSINESS,
    HIRE_MANAGER,
    SAVE_DATA,
    RESET,
}
