import { useEffect, useRef } from 'react';

const useInterval = (cb: () => void, delay: number | null): number => {
    const intervalRef = useRef(-1);
    const callbackRef = useRef(cb);

    useEffect(() => {
        callbackRef.current = cb;
    }, [cb]);

    useEffect(() => {
        if (delay !== null) {
            intervalRef.current = window.setInterval(() => callbackRef.current(), delay);

            return () => window.clearInterval(intervalRef.current);
        }

        return () => null;
    }, [delay]);

    return intervalRef.current;
};

export default useInterval;
