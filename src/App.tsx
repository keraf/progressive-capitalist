import React, { useState, useCallback, useEffect } from 'react';
import { render } from 'react-dom';
import { Workbox } from 'workbox-window';
import classnames from 'classnames/bind';

// Components
import Header from 'components/header/Header';
import Businesses from 'components/businesses/Businesses';
import Managers from 'components/managers/Managers';
import Settings from 'components/settings/Settings';
import Egg from 'components/egg/Egg';

// Web Worker
// eslint-disable-next-line import/no-webpack-loader-syntax
import Worker from 'worker-loader!./Worker';

// Resources
import { Views, MessageTypes } from 'resources/constants';

// Custom hooks
import useKonamiCode from 'hooks/useKonamiCode';

// Styles
import styles from './App.scss';

// Service worker
if ('serviceWorker' in navigator) {
    window.addEventListener('load', () => {
        const wb = new Workbox('/sw.js');
        wb.register();
    });
}

// Get from localStorage
const savedData: State = JSON.parse(localStorage.getItem('state') || '{}');

// Web Worker
const worker = new Worker();

const cx = classnames.bind(styles);

// Main app
const App: React.FunctionComponent = () => {
    const [isLoading, setIsLoading] = useState(true);
    const [view, setView] = useState(Views.BUSINESSES);
    const [isKonami, setIsKonami] = useKonamiCode();

    // Game state
    const [playerBusinesses, setPlayerBusinesses] = useState<ReadonlyArray<PlayerBusiness>>([]);
    const [playerMoney, setPlayerMoney] = useState<number>(0);

    // Calculate proceedings
    useEffect(() => {
        worker.postMessage({ type: MessageTypes.INIT, value: savedData });
        worker.onmessage = (event) => {
            const { type, value } = event.data;

            switch (type) {
                case MessageTypes.LOADING:
                    setIsLoading(value);
                    return;
                case MessageTypes.SET_MONEY:
                    setPlayerMoney(value);
                    return;
                case MessageTypes.SET_BUSINESSES:
                    setPlayerBusinesses(value);
                    return;
                case MessageTypes.SAVE_DATA:
                    localStorage.setItem('state', value);
                    return;
                default:
                    // eslint-disable-next-line no-console
                    console.warn('Unsupported message', type);
            }
        };
    }, []);

    // Automatic reset of konami code
    useEffect(() => {
        if (isKonami) {
            setTimeout(() => setIsKonami(false), 10000);
        }
    }, [isKonami]);

    // Set view
    const handleSetView = useCallback((e) => {
        const { id } = e.currentTarget.dataset;

        setView(Number(id));
    }, []);

    // Buy businesses
    const handleBuyBusiness = useCallback((e) => {
        const { id, price } = e.currentTarget.dataset;

        worker.postMessage({
            type: MessageTypes.ADD_BUSINESS,
            value: {
                id,
                price,
            },
        });
    }, []);

    // Manually triggering work
    const handleWorkBusiness = useCallback((e) => {
        const { id } = e.currentTarget.dataset;

        worker.postMessage({
            type: MessageTypes.RUN_BUSINESS,
            value: {
                id,
            },
        });
    }, []);

    // Hire manager
    const handleHireManager = useCallback((e) => {
        const { id, price } = e.currentTarget.dataset;

        worker.postMessage({
            type: MessageTypes.HIRE_MANAGER,
            value: {
                id,
                price,
            },
        });
    }, []);

    // Reset game
    const handleResetGame = useCallback(() => {
        worker.postMessage({ type: MessageTypes.RESET });
    }, []);

    if (isLoading) {
        return <span>Loading...</span>;
    }

    return (
        <div className={cx('wrapper')}>
            <Header
                money={playerMoney}
                onSetView={handleSetView}
                view={view}
            />
            {view === Views.BUSINESSES && (
                <Businesses
                    playerBusinesses={playerBusinesses}
                    playerMoney={playerMoney}
                    onBuyBusiness={handleBuyBusiness}
                    onWorkBusiness={handleWorkBusiness}
                />
            )}
            {view === Views.MANAGERS && (
                <Managers
                    playerBusinesses={playerBusinesses}
                    playerMoney={playerMoney}
                    onHireClick={handleHireManager}
                />
            )}
            {view === Views.SETTINGS && (
                <Settings onClickReset={handleResetGame} />
            )}
            {isKonami && (
                <Egg />
            )}
        </div>
    );
};

render(<App />, document.getElementById('app'));
