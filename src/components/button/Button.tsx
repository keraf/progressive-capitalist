/* eslint-disable react/jsx-props-no-spreading */

import React from 'react';
import classnames from 'classnames/bind';

import styles from './Button.scss';

interface Props {
    readonly children: any;
    readonly type: string;
    readonly isDisabled?: boolean;
    readonly onClick: (event: React.MouseEvent<HTMLButtonElement>) => void;
}

const cx = classnames.bind(styles);

const Button: React.FunctionComponent<Props> = ({
    children,
    type,
    isDisabled,
    onClick,
    ...rest
}) => (
    <button
        className={cx('btn', type)}
        disabled={isDisabled}
        onClick={onClick}
        type="button"
        {...rest}
    >
        { children }
    </button>
);

Button.defaultProps = {
    type: 'success',
    isDisabled: false,
};

export default Button;
