import React, { useMemo } from 'react';
import classnames from 'classnames/bind';

import Manager from 'components/manager/Manager';

import getConfig from 'resources/config';
import { getBusinessById } from 'resources/business';

import styles from './Managers.scss';

interface Props {
    readonly playerBusinesses: ReadonlyArray<PlayerBusiness>;
    readonly playerMoney: number;
    readonly onHireClick: (event: React.MouseEvent<HTMLButtonElement>) => void;
}

const cx = classnames.bind(styles);
const { businesses } = getConfig();

const Managers: React.FunctionComponent<Props> = ({
    playerBusinesses,
    playerMoney,
    onHireClick,
}) => {
    const managers = useMemo(() => businesses.map((b) => {
        const playerBusiness = getBusinessById(b.id, playerBusinesses);

        return {
            businessId: b.id,
            name: b.manager.name,
            description: b.manager.description,
            price: b.manager.price,
            hasBusiness: playerBusiness !== undefined,
            isHired: playerBusiness === undefined ? false : playerBusiness.hasManager,
        };
    }), [playerBusinesses]);

    return (
        <div className={cx('wrapper')}>
            {managers.map((manager) => (
                <Manager
                    key={manager.businessId}
                    businessId={manager.businessId}
                    name={manager.name}
                    description={manager.description}
                    price={manager.price}
                    hasBusiness={manager.hasBusiness}
                    isHired={manager.isHired}
                    playerMoney={playerMoney}
                    onHireClick={onHireClick}
                />
            ))}
        </div>
    );
};

export default Managers;
