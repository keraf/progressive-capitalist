import React from 'react';
import classnames from 'classnames/bind';

import Button from 'components/button/Button';

import getConfig from 'resources/config';
import { abvNumber } from 'resources/money';

import managerImages from 'images/managers';

import styles from './Manager.scss';

interface Props {
    readonly businessId: string;
    readonly name: string;
    readonly description: string;
    readonly price: number;
    readonly hasBusiness: boolean;
    readonly isHired: boolean;
    readonly playerMoney: number;
    readonly onHireClick: (event: React.MouseEvent<HTMLButtonElement>) => void;
}

const cx = classnames.bind(styles);
const { currency } = getConfig();

const Manager: React.FunctionComponent<Props> = ({
    businessId,
    name,
    description,
    price,
    hasBusiness,
    isHired,
    playerMoney,
    onHireClick,
}) => (
    <div className={cx('wrapper')}>
        <div className={cx('avatar')}>
            <img
                src={managerImages[businessId] || ''}
                alt={`${name}'s avatar`}
            />
        </div>
        <div className={cx('infos')}>
            <span className={cx('name')}>
                { name }
                &nbsp;|&nbsp;
                { abvNumber(price) }
                &nbsp;
                { currency }
            </span>
            <span className={cx('description')}>{ description }</span>
        </div>
        <Button
            type={((price > playerMoney) || !hasBusiness) && !isHired ? 'error' : 'success'}
            isDisabled={(price > playerMoney) || !hasBusiness || isHired}
            onClick={onHireClick}
            data-id={businessId}
            data-price={price}
        >
            { isHired ? 'Hired' : 'Hire!'}
        </Button>
    </div>
);

export default Manager;
