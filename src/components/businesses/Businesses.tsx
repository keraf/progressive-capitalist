import React from 'react';
import classnames from 'classnames/bind';

import BusinessComponent from 'components/business/Business';

import getConfig from 'resources/config';

import styles from './Businesses.scss';

interface Props {
    readonly playerBusinesses: ReadonlyArray<PlayerBusiness>;
    readonly playerMoney: number;
    readonly onBuyBusiness: (event: React.MouseEvent<HTMLButtonElement | HTMLDivElement>) => void;
    readonly onWorkBusiness: (event: React.MouseEvent<HTMLButtonElement>) => void;
}

const cx = classnames.bind(styles);
const { businesses } = getConfig();

const Businesses: React.FunctionComponent<Props> = ({
    playerBusinesses,
    playerMoney,
    onBuyBusiness,
    onWorkBusiness,
}) => (
    <div className={cx('wrapper')}>
        { businesses.map((business: Business) => (
            <BusinessComponent
                key={business.id}
                id={business.id}
                name={business.name}
                price={business.price}
                revenue={business.revenue}
                time={business.time}
                playerMoney={playerMoney}
                playerBusinesses={playerBusinesses}
                onBuyBusiness={onBuyBusiness}
                onWorkBusiness={onWorkBusiness}
            />
        )) }
    </div>
);

export default Businesses;
