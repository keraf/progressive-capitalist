import React from 'react';
import classnames from 'classnames/bind';

import Button from 'components/button/Button';

import styles from './Settings.scss';

interface Props {
    readonly onClickReset: (event: React.MouseEvent<HTMLButtonElement>) => void;
}

const cx = classnames.bind(styles);

const Settings: React.FunctionComponent<Props> = ({
    onClickReset,
}) => (
    <div className={cx('wrapper')}>
        <Button
            type="error"
            onClick={onClickReset}
        >
            Reset game
        </Button>
        <div className={cx('credits')}>
            Icons made by&nbsp;
            <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a>
            {' and '}
            <a href="https://www.flaticon.com/authors/surang" title="surang">surang</a>
            {' from '}
            <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>
        </div>
    </div>
);

export default Settings;
