import { MessageTypes } from 'resources/constants';
import getConfig from 'resources/config';
import {
    addBusinessToPlayer,
    addManagerToPlayerBusiness,
    updateLastStart,
    getBusinessById,
} from 'resources/business';
import { calculateEarnings } from 'resources/money';

// eslint-disable-next-line no-restricted-globals
const ctx: Worker = self as any;
let player: State = {
    businesses: [],
    money: -1,
};

const {
    businesses,
    startMoney,
} = getConfig();

// Post data to parent thread
const msg = (type: MessageTypes, value: any): void => ctx.postMessage({
    type,
    value,
});

const setMoneyAndBusiness = (
    playerBusinesses: ReadonlyArray<PlayerBusiness>,
    playerMoney: number,
    save = true,
): void => {
    player = {
        businesses: playerBusinesses,
        money: playerMoney,
    };

    msg(MessageTypes.SET_BUSINESSES, player.businesses);
    msg(MessageTypes.SET_MONEY, player.money);

    if (save) {
        msg(MessageTypes.SAVE_DATA, JSON.stringify(player));
    }
};

const messageAction = (type: MessageTypes, value: any): void => {
    switch (type) {
        case MessageTypes.INIT: {
            // The value passed is the saved state
            player = { ...value };

            if (player.businesses === undefined && player.money === undefined) {
                setMoneyAndBusiness([], startMoney);
                msg(MessageTypes.LOADING, false);

                return;
            }

            // Calculate earnings while offline
            let earningsSinceLastTime = 0;

            const newPlayerBusinesses = player.businesses.map((playerBusiness) => {
                // Checking if the player has a manager or not because it could happen that
                // the web worker gets killed before it was able to set a non-null value to
                // `lastStart`. Without this check, there is a small chance of the business
                // getting stuck forever.
                if (playerBusiness.lastStart === null && !playerBusiness.hasManager) {
                    return playerBusiness;
                }

                const business = businesses.find((b) => b.id === playerBusiness.id);
                if (business === undefined) {
                    return playerBusiness;
                }

                const currentTime = +new Date();
                if (!playerBusiness.hasManager) {
                    const targetTime = playerBusiness.lastStart + (business.time * 1000);

                    if (targetTime - currentTime > 0) {
                        messageAction(MessageTypes.RUN_BUSINESS, {
                            id: playerBusiness.id,
                            time: targetTime - currentTime,
                        });

                        return playerBusiness;
                    }

                    earningsSinceLastTime += calculateEarnings(business.revenue, playerBusiness.count);

                    return {
                        ...playerBusiness,
                        lastStart: null,
                    };
                }

                const businessRuns = ((currentTime - playerBusiness.lastStart) / business.time) / 1000;
                const totalRuns = Math.floor(businessRuns);
                const progressLeft = 100 - ((businessRuns - totalRuns) * 100);
                const timeLeft = (progressLeft / 100) * business.time;

                messageAction(MessageTypes.RUN_BUSINESS, {
                    id: playerBusiness.id,
                    time: timeLeft,
                });

                earningsSinceLastTime += calculateEarnings(business.revenue, playerBusiness.count) * businessRuns;

                return {
                    ...playerBusiness,
                    lastStart: currentTime - Math.round(timeLeft * 1000),
                };
            });

            // Set
            const newPlayerMoney = value.money + Math.round(earningsSinceLastTime);
            setMoneyAndBusiness(newPlayerBusinesses, newPlayerMoney);

            // Set loading to false
            msg(MessageTypes.LOADING, false);
            return;
        }
        case MessageTypes.ADD_BUSINESS: {
            const { id, price } = value;

            if (price > player.money) {
                return;
            }

            const business = businesses.find((b) => b.id === id);
            if (business === undefined) {
                return;
            }

            const newPlayerBusinesses = addBusinessToPlayer(business, player.businesses);
            const newPlayerMoney = player.money - Number(price);

            setMoneyAndBusiness(newPlayerBusinesses, newPlayerMoney);
            return;
        }
        case MessageTypes.RUN_BUSINESS: {
            const { id, time } = value;

            const business = businesses.find((b) => b.id === id);
            if (business === undefined) {
                return;
            }

            const playerBusiness = getBusinessById(id, player.businesses);
            if (playerBusiness === undefined) {
                return;
            }

            if (playerBusiness.lastStart !== null && time === undefined) {
                return;
            }

            if (time === undefined) {
                const newPlayerBusinesses = updateLastStart(id, player.businesses);
                setMoneyAndBusiness(newPlayerBusinesses, player.money);
            }

            setTimeout(() => {
                const pBusiness = getBusinessById(id, player.businesses);
                if (pBusiness === undefined) {
                    return;
                }

                const earnings = calculateEarnings(business.revenue, pBusiness.count);
                const newPlayerMoney = player.money + earnings;
                const newPlayerBusinesses = updateLastStart(id, player.businesses, null);

                setMoneyAndBusiness(newPlayerBusinesses, newPlayerMoney);

                if (pBusiness.hasManager) {
                    messageAction(MessageTypes.RUN_BUSINESS, { id });
                }
            }, (time || (1000 * business.time)));

            return;
        }
        case MessageTypes.HIRE_MANAGER: {
            const { id, price } = value;

            if (price > player.money) {
                return;
            }

            messageAction(MessageTypes.RUN_BUSINESS, { id });

            const newPlayerBusinesses = addManagerToPlayerBusiness(id, player.businesses);
            const newPlayerMoney = player.money - Number(price);

            setMoneyAndBusiness(newPlayerBusinesses, newPlayerMoney);
            return;
        }
        case MessageTypes.RESET: {
            setMoneyAndBusiness([], startMoney);
            return;
        }
        default:
            // eslint-disable-next-line no-console
            console.warn('Unsupported Worker message', type);
    }
};

// Respond to message from parent thread
ctx.addEventListener('message', (event) => {
    const { type, value } = event.data;

    messageAction(type, value);
});
