const fs = require('fs');
const path = require('path');
const webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WebpackPwaManifest = require('webpack-pwa-manifest');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const WorkboxPlugin = require('workbox-webpack-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

const { NODE_ENV, WEBPACK_DEV_SERVER } = process.env;

const environment = NODE_ENV || 'development';
const pkgFile = require('./package.json');
const isDev = environment === 'development';

module.exports = {
    context: __dirname,
    mode: environment,
    devtool: isDev ? 'source-map' : false,
    devServer: {
        writeToDisk: true,
    },
    entry: {
        main: path.join(__dirname, 'src', 'App.tsx'),
    },
    output: {
        path: path.join(__dirname, 'public'),
        publicPath: '/',
        filename: 'js/[name].[hash:8].js',
        chunkFilename: 'js/[id].[hash:8].js',
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx', '.scss'],
        modules: [
            path.join(__dirname, 'src'),
            path.resolve('node_modules'),
        ],
    },
    stats: {
        children: false,
        modules: false,
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                vendor: {
                    chunks: 'initial',
                    name: 'vendor',
                    test: /[\\/]node_modules[\\/]/,
                },
            },
        },
    },
    module: {
        rules: [
            // Javascript
            {
                test: /\.(j|t)sx?$/,
                exclude: /node_modules/,
                use: 'babel-loader',
            },
            {
              enforce: 'pre',
              test: /\.js$/,
              loader: 'source-map-loader',
            },
            // Styles
            {
                test:/\.s?css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            modules: {
                                mode: 'local',
                                localIdentName: isDev ? '[name]__[local]' : '[hash:base64]',
                            },
                            sourceMap: isDev,
                        },
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            // outputStyle: 'compressed',
                            sourceMap: isDev,
                        },
                    },
                ],
            },
            // Images
            {
                test: [/\.gif$/, /\.jpe?g$/, /\.png$/],
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 10000,
                        name: 'img/[name].[hash:8].[ext]',
                    },
                },
            },
        ],
    },
    plugins: [
        new CleanWebpackPlugin(),
        new webpack.DefinePlugin({
            VERSION: JSON.stringify(pkgFile.version),
        }),
        new WebpackPwaManifest({
            filename: 'manifest.[hash:8].json',
            name: 'Progressive Capitalist',
            short_name: 'ProCap',
            description: '',
            theme_color: '#FFEC49',
            background_color: '#ffffff',
            orientation: 'portrait',
            scope: '/',
            inject: true,
            icons: [
                {
                    src: path.join(__dirname, 'src', 'images', 'icon.png'),
                    sizes: [96, 128, 192, 256, 384, 512],
                    destination: 'img',
                },
            ],
        }),
        new MiniCssExtractPlugin({
            filename: 'css/[name].[hash:8].css',
            chunkFilename: 'css/[id].[hash:8].css',
        }),
        new HtmlWebpackPlugin({
            minify: isDev,
            meta: {
                viewport: 'width=device-width, initial-scale=1, maximum-scale=5, shrink-to-fit=no',
                description: 'A PWA clone of Adventure Capitalist.',
            },
            favicon: path.join(__dirname, 'src', 'static', 'favicon.png'),
            template: path.join(__dirname, 'src', 'static', 'index.html'),
            templateParameters: {
                APP_NAME: 'Progressive Capitalist',
            },
        }),
        new WorkboxPlugin.GenerateSW({
            swDest: 'sw.js',
            clientsClaim: true,
            skipWaiting: true,
            runtimeCaching: [{
                urlPattern: new RegExp(/\/(\?s=[a-zA-Z0-9=]+)?/g),
                handler: 'StaleWhileRevalidate',
            }],
        }),
        WEBPACK_DEV_SERVER ? new BundleAnalyzerPlugin({
            openAnalyzer: false,
        }) : () => null,
    ],
};
