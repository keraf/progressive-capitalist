# Progressive Capitalist
> The following application has been developed as part of an interview exercise.

Progressive Capitalist is a progressive web app (PWA) clone of the famous game Adventure Capitalist. This is not a 1:1 replica of that game but implements similar mechanics.

You can try it here: https://procap.ker.af/

**Screenshots**
![Running as an installed PWA on Chrome Desktop](https://gitlab.com/keraf/progressive-capitalist/-/raw/master/doc/game-pc.jpg)

![Running as an installed PWA on Firefox Mobile](https://gitlab.com/keraf/progressive-capitalist/-/raw/master/doc/game-mobile.jpg)


## Behind the curtain
### Technologies & APIs
For this project, the following technologies and APIs have been used:
- React
- TypeScript
- Babel
- Webpack
- ESLint
- LocalStorage
- Web/Service Workers

### Technical choices
#### Choosing DOM over Canvas
Canvas is a great technology for the web, especially when developing games due to the performance that can be achieved for displaying non structured elements and animations. In the case of this game, the display is relatively simple and structured. Using the DOM allows me to easily take advantage of responsive features, allowing me to quickly develop a desktop and mobile in one go.

When it comes to working with the DOM, my library of choice is React as I have a fair share of experience with it and I'm able to work productively with it.

#### TypeScript
TypeScript enriches the developer experience with many language features and tooling. The advantages of using TypeScript are listed all over the web but the ones that matter the most to me are the use of Types (obviously) which provide immediate feedback, resulting in less mistakes. In addition, the integration with Visual Studio Code is great and makes it a breeze to work with it. 

#### Client Side Only
Even though I'm a full stack engineer, I deliberately chose to make this game client side only. This decision was motivated by multiple factors:
- There's no need for an online service as no accounts are needed and no leaderboards or interactions between players are part of the game.
- Having an entirely client-side application makes it easy to support offline play as all the code needed runs on the user's device.
- The output files are static, meaning they can be deployed at a very low cost for high volumes of traffic. It can even be deployed on a CDN to improve loading times in most parts of the world.

Having all the code running on the client, and with the requirement for revenue to be generated while the user has the game closed, I came up with the idea of recording the last starting time of when a business was run, wether it has a manager or not. Using this, when coming back to the game, I can figure out how much time passed which will then get me the amount of times the business ran (multiplying the earnings) and what is the current progress on it.

I can only think of one minor disadvantage (there are probably others!) which is cheating. Players can edit their localStorage to increase money, unlock businesses or hire managers for free. But as all the stats are local, the user will only impact themselves and there's no competitive advantage for them.
 
#### Progressive Web App
With all the code being executed on the client side, it makes sense to enrich the user's experience by making it a PWA. Offline support and installable are among the advantages that PWAs offer. A user can now install the game on his device using his browser (Chrome & Firefox on Android, Safari on iOS) and play during a flight for example.

#### Web Workers
As JavaScript runs in a single thread responsible for the UI, heavy operations can end up locking the interface and provide a bad user experience. Thanks to Web Workers, work can be sent to a separate thread and run asynchronously. The way I architected it for this game, is that the Web Worker is some sort of API that is responsible for managing the player state (how much money and what businesses they have). Every message sent to the Worker has a type and optionally a value. This will execute some work which will then dispatch messages for the client (update money or businesses). The client will handle certain types of incoming messages from the Worker and set the states in the UI accordingly.

This approach also allows me to keep the role of React to presentation while the Workers do the game state operations. 

#### Aiming for performance
There are multiple aspects that determine the performance of the application, runtime, bundle size, caching, compression, DNS resolution, etc... As a developer, I created this game on a powerful machine with high speed fiber internet, but I have to keep in mind that most people in the world will access my game in different conditions. People in different parts of the world have hardware ranging from cheese graders to super computers and connections from potato 2G to 10Gb fiber. In addition of providing a better user experience, aiming for performance increases reach. 

The first goal was to provide good runtime performance. React, when used well, can be really performant. Part of keeping the UI reactive is also not overloading it with complex operations. This is a choice that also motivated me to use Web Workers to delegate some work to a different thread.

Providing a small bundle size was my second goal. Being able to download the application rapidly, even on slow connections, must not be neglected. This was achieved by keeping dependencies low, analyzing where the bloat comes from (see "How to? -> Analyze" below), and a properly configured production build (with Webpack). 

Thirdly are aspects that have to do with the distribution: Hosting, caching rules, compression, etc... As I only have static files, using Gitlab Pages is a cost efficient (free!) solution. I also decided to use CloudFlare as they provide useful services for little to no cost. Their DNS resolutions are fast and proxying the traffic through their infrastructure provides many advantages (CDN, caching, and other performance optimizations).

### Hosting
The application is hosted on Gitlab Pages. Using Gitlab's CI/CD pipeline, every commit in master creates a new build that is automatically pushed to the live application.

### Performance results
Performance results are for the hosted site running a production build. These are obtained using test tools. Manual tests might highlight issues that could have been missed by these tools.

#### Lighthouse
![Lighthouse Results](https://gitlab.com/keraf/progressive-capitalist/-/raw/master/doc/lighthouse.jpg)

#### PageSpeed Insights
- [99% on Mobile](https://developers.google.com/speed/pagespeed/insights/?hl=en&url=https%3A%2F%2Fprocap.ker.af%2F&tab=mobile)
- [100% on Desktop](https://developers.google.com/speed/pagespeed/insights/?hl=en&url=https%3A%2F%2Fprocap.ker.af%2F&tab=desktop)

![Insights Mobile](https://gitlab.com/keraf/progressive-capitalist/-/raw/master/doc/insight-mobile.jpg)
![Insights Desktop](https://gitlab.com/keraf/progressive-capitalist/-/raw/master/doc/insight-desktop.jpg)

### There's a secret
> Up Down Up Do... No that's not it. Up Up Down Down? Hmmm... What's after that?

### Personal notes
- Would have been cool to try Web Assembly for this project. Having experience with .NET and C#, the obvious choice for a framework would have been Blazor. But I didn't wanted to risk using tool I'm unfamiliar with for very little tradeoff. React did the job well but maybe in the future I'll make a Web Assembly Capitalist version :smile: 
- English is my third language, so be ready to find spelling or grammar mistakes. Sorry about that!

## How to?
### Get started
First of all, you need NodeJS and NPM (if you don't have it already). Then run the command `npm i` at the root of the project in order to install all dependencies. And that's basically it! You're now ready to develop, build, test and deploy the project!

### Configure
A config file at the root of the project (`config.json`) will allow you to tweak most aspects of the game. You can for example update the values of businesses and their manager, or the start money, or maximum number of businesses of one type you can own...

Here are the details
#### Root
| Name | Type | Description |
| --- | --- | --- |
| currency | string | Currency symbol. For example: $. |
| startMoney | number | Starting money. |
| maxBusinesses | number | Maximum of businesses from one type you can own. |
| revenueIncrease | number | Amount of increase in the revenue for each business owned. |
| priceIncrease | number | Amount of increase in the price after each business bought. |
| businesses | Array of Business | All businesses that can be used (see details below). |

#### Root -> Business
| Name | Type | Description |
| --- | --- | --- |
| id | string | Unique identifier. |
| name | string | Name of the business. |
| price | number | Start price for that business. Increase is calculated based on the `priceIncrease` configuration and amount of businesses of that type owned. |
| revenue | number | Start revenue for that business. Increase is calculated based on the `revenueIncrease` configuration and amount of businesses of that type owned. |
| time | number | Time it takes to get the revenue. |
| manager | Manager | Configuration of that business' manager (see details below). |

#### Root -> Business -> Manager
| Name | Type | Description |
| --- | --- | --- |
| name | string | Name of the manager. |
| description | string | Description for the manager. Be creative :wink: |
| price | number | Cost for hiring the manager. |

### Develop
All source files are locate in the `src/` folder. Using your favorite code editor, edit the files in there and run `npm run watch:dev` to run Webpack in watch mode and launch a development server (by default at http://localhost:8080). You can also run the same command but with production parameter with `npm run watch:prod`.

### Analyse
You can analyse the runtime using your favorite browser dev tools (shout out to Firefox Developer Edition) by profiling interactions and taking a memory snapshot. But there's another tool you might find hand for checking the build sizes! When running any of the `watch` commands (see above), Webpack Bundle Analyzer will run as well. You can analyze your bundle sizes at http://localhost:8888.

### Build
You can build for development (includes useful development features) or production (gets rid of anything unnecessary to produce a small and fast build). For a dev build, run `npm run build:dev` and for a prod build, run `npm run build:prod`. You will find the output in the `public/` folder.

### Test
~~Simply run `npm test` and a series of unit tests will run.~~ Coming soon!

### Lint
Simply run `npm lint` to run ESLint on the project. Code Editors can (with an optional plugin) highlight linting issues automatically.

### Deploy
Just commit to the master branch, and Gitlab will automatically run the deployment pipeline. ~~It will only successfully deploy if all tests are passing! So make sure you don't break anything before you commit~~. You should then see your changes [live](https://procap.ker.af) :smile:

## What's next?
You might find some issues in the Issue Tracker. But on the top of my head, and with a bit more time, these would be the next areas of focus (not in order).

### Tests
Ran out of time but testing is definitely next on the list. Especially since commits to master automatically make it to the live site. There should be additional safety nets.

### Better UI/UX
I am not a great UI designer and I end up spending more time on it than I should for an OK-ish result. Ideally, the UI should be better but at least the underlying code works and it shouldn't be too much trouble updating styles without touching too much the rest of the code.

### Accessibility
Leave no one behind and make sure the game can be played by anyone.

### Sounds & Vibrations
Make the experience richer, add sounds to provide some auditive feedback. And also vibrations for mobile users.

### Animations
Add more life to the game. Animate elements, throw bills all over the screen, etc... 

### Figure out a more sensible way for price & revenue increase
Currently, the prices and revenue are increasing linearly based on a configuration value. It seems that Ad Capitalist doesn't have a linear progression. With a bit more time, I could probably figure out the math behind it.

### Get the values right
Game design is a job by itself. The values don't feel right but the implementation is here. Just need to find someone with enough knowledge and patience to tweak the values in the `config.json` file.

### Use SVGs instead of PNGs
Most images I found are PNGs but they are not very complex and could be provided as SVGs. That would allow better scalability.

### More features
With a little more time, I would implement more features from Ad Capitalist, such as:
- Tutorial (not very obvious that to do if you haven't played AdCap before)
- Boosts (those x3 boosters you can buy in the shop)
- Welcome back screen (where profits while AFK are shown)
- Multi buy button (you can buy multiple businesses at once)
- Achievements (they are fun, why not)
- Increase production speed when buying more stores 

### Handling edge-cases
Extensive testing will likely uncover some edge-cases. A few I can think of, and their solution, would be:
- Playing the game in two different tabs would overwrite the saved local state.
  - Solution: Moving the Web Worker to a Shared Worker and broadcast the events across tabs.
- The user device time getting changed. 
  - Solution: Maybe it's already automagically handled?
- Race conditions in the Worker might result data loss.
  - Solution: Create a queue for incoming events to be treated one after another.

### Documentation
Documenting code is important when working collaboratively. Using JS-Doc comments above functions to give a better understanding of that the function does, the parameters it accepts and what it returns.

### Refactoring
Globally, I am pretty happy with the code quality / amount of time available ratio but there's always room for improvements with more time. A few things I would like to rework would be:
- Isolating more parts of the application to make testing easier.
- ...

### Fix bugs
There are some known bugs:
- Not enough space on the buy and work buttons on small screens making the text break into two lines.
- ...

### Better dev environments
Create a VS Code workspace file with some shared configurations. It would also be nice to use the Run/Debug features within VS Code to execute and debug the project.
